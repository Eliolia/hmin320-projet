﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySoundMenu : MonoBehaviour
{
    public SettingsParameters settings;
    public AudioSource audioS;

    // Start is called before the first frame update
    void Start()
    {
        settings = GameObject.FindGameObjectWithTag("Settings").GetComponent<SettingsParameters>();
        audioS = GameObject.Find("AudioSourceHandler").GetComponent<AudioSource>();
    }

    public void PlayValidate()
    {
        audioS.PlayOneShot(settings.SoundData.validate, settings.GameSettings.sfxVolume);
    }
    public void PlayCollide()
    {
        audioS.PlayOneShot(settings.SoundData.collide, settings.GameSettings.sfxVolume);
    }
    public void PlayBack()
    {
        audioS.PlayOneShot(settings.SoundData.back, settings.GameSettings.sfxVolume);
    }
    public void PlayArrows()
    {
        audioS.PlayOneShot(settings.SoundData.arrows, settings.GameSettings.sfxVolume);
    }
    public void PlayBackGround()
    {
        audioS.PlayOneShot(settings.SoundData.backgroundMenu, settings.GameSettings.sfxVolume);
    }
}
