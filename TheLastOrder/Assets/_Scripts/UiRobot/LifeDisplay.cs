﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LifeDisplay : MonoBehaviour
{
    [SerializeField] protected Sprite heartSprite;
    [SerializeField] protected float spacing = 0.15f;

    private void Start() {
        ClearHeart();
    }

    public void SetCurrentLife(int nbLife){
        ClearHeart();
        for(int i = 0; i < nbLife; i++){
            GenerateHeart(i);
        }
    }

    protected void ClearHeart(){
        foreach (Transform child in transform) {
            GameObject.Destroy(child.gameObject);
        }
    }

    protected GameObject GenerateHeart(int position = 0){
        GameObject heart = new GameObject("Heart");
        heart.transform.parent = transform;

        Image img = heart.AddComponent<Image>();
        RectTransform rect = heart.GetComponent<RectTransform>();
        img.sprite = heartSprite;
        rect.sizeDelta = new Vector2(0.14f, 0.14f);
        rect.anchorMin = new Vector2(0, 0.5f);
        rect.anchorMax = new Vector2(0, 0.5f);
        rect.anchoredPosition3D = new Vector3(position * spacing, 0, 0);
        rect.localRotation = Quaternion.identity;

        return heart;
    }
}
