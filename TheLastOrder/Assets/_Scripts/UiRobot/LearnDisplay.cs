﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class LearnDisplay : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI text;
    // Start is called before the first frame update
    public void OnInstruction(OrderTracker order) {
        text.text = order.currentLearn + " " + order.currentNumberLearn;
    }
}
