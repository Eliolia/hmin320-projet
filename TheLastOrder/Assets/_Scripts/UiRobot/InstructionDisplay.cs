﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class InstructionDisplay : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI text;
    // Start is called before the first frame update
    public void OnInstruction(OrderTracker order) {
        text.text = order.currentInstruction;
    }
}
