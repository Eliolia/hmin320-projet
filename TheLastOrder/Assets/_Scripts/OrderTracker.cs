﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using UnityEngine.Android;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class OrderTracker : MonoBehaviour
{
    public const float Epsilon = 1.401298E-45f;
    readonly public static double I0 = 0.00002; //Accoustic reference 2*10^-5

    [Range(0.0f, 0.1f)]
    [SerializeField] float confidence = 0.03f;
    [Range(0.0f, 0.1f)]
    [SerializeField] float lastconfidence = 0.03f;
    [SerializeField] bool DEBUG_MODE = false;
    [SerializeField] int trainTime = 3;

    [System.Serializable]
    public struct Instruction {
        public string name;
        public EventTrigger.TriggerEvent actions;
    }

    [SerializeField] protected bool autoLoadSaves = false;

    protected string microphone;

    [SerializeField] Text uiText;

    float timerRestart = 0.0f;
    float timeRestart = 0.1f;
    bool haveRestarted = true;

    float timeLoopControl = 0.5f;
    float timerLoopControl = 0.0f;

    bool isRunning = false;
    bool isRunningSave = false;
    bool speachRunning = false;
    bool speachRunningSave = false;

    float timeControlRunning = 5.0f;
    float timerControlRunning = 0.0f;

    float timeRemoveInstructionDebug = 3.0f;
    float timerRemoveInstructionDebug = 0.0f;

    //Instruction perceive
    string currentFinalText = "";
    string currentPartialText = "";
    string previousPartialText = "";

    [Range(0, 1)]
    public float currentDB = 0;
    [Range(0, 1)]
    public float getDB = 0;
    List<float> averageDB = new List<float>();

    //Last instruction get
    public string currentInstruction = "";

    public string currentLearn = "";
    public int currentNumberLearn = 0;

    //DictionnaryInstructions
    [SerializeField]
    public List<Instruction> instructions = new List<Instruction>();
    List<string> instructionDebugList = new List<string>();
    bool previousAccepted = false;

    [Range(0, 40)]
    [SerializeField] int nbChunck = 0;
    [Range(0, 40)]
    [SerializeField] int chunckWait = 0;
    [Range(0, 40)]
    [SerializeField] int chunckErrorCount = 0;
    [Range(0, 40)]
    [SerializeField] int chunckErrorAcceleration = 2;

    List<GameObject> audioViewer = new List<GameObject>();
    List<GameObject> audioViewer2 = new List<GameObject>();
    int debugCount = 512;
    float sizeDebug;
    float[] tmpDataDebug = new float[512];
    float[] dataDebug = new float[512];
    float[] dataDebug_OLD = new float[512];
    float[] sdata = new float[512];

    int wantedSpectrum = 64;
    float sizeSpectrum;

    int wantedCount = 512;
    float percentageWanted = 0.1f;
    float sizeRecord;
    protected AudioSource _audioSource;
    [SerializeField] protected GameObject sliderMicrophone;
    float currentValue = 0f;
    List<float> data = new List<float>();
    List<float> data2 = new List<float>();
    float[] dataTemp = new float[1024];

    Dictionary<string, List<float[]>> actionsModel = new Dictionary<string, List<float[]>>();

    [System.Serializable]
    public class DatasSave {
        public List<float> data = new List<float>();
    }

    [System.Serializable]
    public class VoiceDatasSave {
        public List<DatasSave> datas = new List<DatasSave>();
    }

    [System.Serializable]
    public class VoiceSave {
        public List<VoiceDatasSave> allDatas = new List<VoiceDatasSave>();
        public List<string> commands = new List<string>();
    }

    VoiceSave ConvertToSave(Dictionary<string, List<float[]>> ordersDatas){
        VoiceSave save = new VoiceSave();

        foreach (var command in ordersDatas) {
            save.commands.Add(command.Key);

            save.allDatas.Add(new VoiceDatasSave());
            foreach (var i in command.Value){
                save.allDatas[save.allDatas.Count - 1].datas.Add(new DatasSave());
                foreach(var j in i){
                    save.allDatas[save.allDatas.Count - 1].datas[save.allDatas[save.allDatas.Count - 1].datas.Count - 1].data.Add(j);
                }
            }
        }
        
        return save;
    }

    // LOADING SAVE DATAS
    void SaveVoiceOrder(string saveName, Dictionary<string, List<float[]>> ordersDatas){
        string dataSave = JsonUtility.ToJson(ConvertToSave(ordersDatas));
        Debug.Log(dataSave);
        System.IO.Directory.CreateDirectory(Application.persistentDataPath + "/VoiceDatas/");
        System.IO.File.WriteAllText(Application.persistentDataPath + "/VoiceDatas/" + saveName + ".json", dataSave);
    }

    VoiceSave LoadVoiceOrder(string saveName){
        string path = Application.persistentDataPath + "/VoiceDatas/" + saveName + ".json";
        if (System.IO.File.Exists(path)) {
            string fileContent = System.IO.File.ReadAllText(path);
            VoiceSave save = JsonUtility.FromJson<VoiceSave>(fileContent);
            return save;
        }
        return null;
    }

    void ApplyVoiceSave(VoiceSave save){
        if(save == null) return;
        actionsModel.Clear();
        int index = 0;
        foreach(var i in save.commands){
            actionsModel.Add(i, new List<float[]>());
            foreach(var datas in save.allDatas[index].datas){
                float[] newDatas = new float[debugCount];
                int indexPos = 0;
                foreach(var data in datas.data){
                    newDatas[indexPos++] = data;
                }
                actionsModel[i].Add(newDatas);
            }
            index++;
        }
    }

    void Awake()
    {
        Debug.Log(Application.persistentDataPath);
        sizeSpectrum = 1f / (wantedSpectrum);
        sizeRecord = 1f / (wantedCount);
        sizeDebug = 1f / (debugCount);

        actionsModel.Add("viens", new List<float[]>());
        actionsModel.Add("pars", new List<float[]>());
        actionsModel.Add("stop", new List<float[]>());
        //actionsModel.Add("pause", new List<float[]>());

        if(autoLoadSaves){
            VoiceSave save = LoadVoiceOrder("default");
            ApplyVoiceSave(save);
        }
    }

    private void Start() {
        for (int i = 0; i < wantedCount; i++) {
            if (DEBUG_MODE) {
                GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
                cube.transform.parent = sliderMicrophone.transform;
                cube.transform.localPosition = new Vector3(i * sizeRecord - (wantedCount / 2f) * sizeRecord, 0, 0);
                cube.transform.localScale = new Vector3(sizeRecord, sizeRecord, sizeRecord);
                cube.transform.localRotation = Quaternion.identity;
                cube.GetComponent<Renderer>().material.color = new Color(1, 1, 1);
                Destroy(cube.GetComponent<BoxCollider>());
                audioViewer.Add(cube);
            }
            data.Add(0);
        }
        for (int i = 0; i < wantedCount; i++) {
            if (DEBUG_MODE) {
                GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
                cube.transform.parent = sliderMicrophone.transform;
                cube.transform.localPosition = new Vector3(i * sizeRecord - (wantedCount / 2f) * sizeRecord, 0, 0.001f);
                cube.transform.localScale = new Vector3(sizeRecord, sizeRecord, sizeRecord);
                cube.transform.localRotation = Quaternion.identity;
                cube.GetComponent<Renderer>().material.color = new Color(1, 0, 0);
                Destroy(cube.GetComponent<BoxCollider>());
                audioViewer2.Add(cube);
            }
            data2.Add(0);
        }

        CheckPermission();
        StartAudioMicrophone();
    }

    void StartAudioMicrophone()
    {
        _audioSource = GetComponent<AudioSource>();
        _audioSource.clip = Microphone.Start(null, true, 1, 44100);
        _audioSource.loop = true; // Set the AudioClip to loop
        while (!(Microphone.GetPosition(null) > 0)) { } // Wait until the recording has started
        _audioSource.Play();
    }

    void StopAudioMicrophone() {
        Microphone.End(null);
    }

    void Update()
    {
        GetAverageVolume();
        uiText.text = "";
        for (int i = 0; i < instructionDebugList.Count && i < 10; i++) uiText.text += instructionDebugList[i] + (i < instructionDebugList.Count - 1 ? "\n" : "");

        if(DEBUG_MODE){
            for (int i = 0; i < data.Count; i++)
            {
                data[i] = Math.Min(1, data[i]);
                data[i] = Math.Max(-1, data[i]);
                // data2[i] = Math.Min(1, data2[i]);
                // data2[i] = Math.Max(-1, data2[i]);
                audioViewer[i].transform.localScale = new Vector3(sizeRecord, 10f * data[i], sizeRecord);
            }

            for(int i = 0; i < data2.Count; i++){
                audioViewer2[i].transform.localScale = new Vector3(sizeRecord, 0.05f, sizeRecord);
                if(!double.IsNaN(data2[i])) audioViewer2[i].transform.localPosition = new Vector3(audioViewer2[i].transform.localPosition.x, data2[i] + 0.05f, audioViewer2[i].transform.localPosition.z);
            }
        }

        timerRestart += Time.deltaTime;
        timerLoopControl += Time.deltaTime;

        if (instructionDebugList.Count > 0)
        {
            timerRemoveInstructionDebug += Time.deltaTime;

            if (timerRemoveInstructionDebug >= timeRemoveInstructionDebug) {
                timerRemoveInstructionDebug = 0.0f;
                instructionDebugList.RemoveAt(0);
            }
        }

        for (int i = 0; i < Input.touchCount; ++i)
        {
            if (Input.GetTouch(i).phase.Equals(TouchPhase.Began)) {
                ResetTrain();
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }
        }

        if(Input.GetKeyDown(KeyCode.A)){
            ResetLastTrain();
        }
    }


    void CheckPermission()
    {
#if UNITY_ANDROID
        if (!Permission.HasUserAuthorizedPermission(Permission.Microphone))
        {
            Permission.RequestUserPermission(Permission.Microphone);
        }
#endif
    }

    void ApplyInstruction(string instruction)
    {
        currentInstruction = instruction;
        instructionDebugList.Add(instruction);
        if (instructionDebugList.Count >= 5)
            instructionDebugList.RemoveAt(0);
        for (int i = 0; i < instructions.Count; i++)
        {
            if (instructions[i].name == instruction) {
                BaseEventData eventData = new BaseEventData(EventSystem.current);
                eventData.selectedObject = this.gameObject;
                instructions[i].actions.Invoke(eventData);
                break;
            }
        }
    }

    float SumArray(List<float> toSum){
        float final = 0.0f;
        for(int i = 0; i < toSum.Count; i++) final += toSum[i];
        return final;
    }

    float GetAverageVolume()
    {
        _audioSource.GetOutputData(dataTemp, 0);
        _audioSource.GetSpectrumData(sdata, 0, FFTWindow.Hamming);
        bool acceptedChunck = false;
        float average = 0f;
        float average_sdata = 0f;
        float max_sdata = 0f;
        int max_sdata_i = 0;
        float db = 0f;
        int nbDB = 0;

        // CalculateFFT(samples, result, false);
        for (int i = 0; i < data2.Count; i++) {
            data2[i] = sdata[i];
        }
        // for (int i = 0; i < 512; i++) {
        //     sdata[i] = data2[i];
        // }
        if (this.data.Count > 0) {
            for (int i = 0; i < dataTemp.Length; i++) {
                average += Math.Abs(dataTemp[i]);
            }
            average /= dataTemp.Length;

            for (int i = 0; i < sdata.Length; i++) {
                if(dataTemp[i] != 0){
                    db += (float)(Math.Log10((dataTemp[i] * dataTemp[i]) / (I0 * I0))) * 0.1f;
                    nbDB++;
                }
                if(sdata[i] > max_sdata){
                    max_sdata = sdata[i];
                    max_sdata_i = i;
                    average_sdata += sdata[i];
                }
            }
            db /= nbDB;
            average_sdata /= sdata.Length;
            average_sdata /= max_sdata;
        }
        // Debug.Log("AVERAGE : " + db);
        if(averageDB.Count >= 26 && chunckWait <= 0 && nbChunck < 50){
            if (db > currentDB * 1.1f + 0.2f) {
                acceptedChunck = true;
                chunckErrorCount = 20;
                chunckErrorAcceleration = 2;
            } else if(chunckErrorCount > 0) {
                acceptedChunck = true;
                if(db > currentDB) chunckErrorCount--;
                else chunckErrorCount-= chunckErrorAcceleration++;
            }
        }
        getDB = db;
        if(!float.IsNaN(db) && !acceptedChunck){ 
            if(averageDB.Count < 25 || db < currentDB * 1.1f) averageDB.Add(db);
            if(averageDB.Count > 256) averageDB.RemoveAt(0);
            List<float> ordered = new List<float>(averageDB);
            ordered.Sort();
            currentDB = ordered[(int)Math.Round((ordered.Count - 1) / 2f)];
        }

        if (acceptedChunck) {
            if(!previousAccepted) ApplyInstruction("INSTRUCTION_LISTEN");
            nbChunck++;
            //Debug.Log("MAX S DATA : " + max_sdata_i + " = " + max_sdata + " | average_sdata = " + average_sdata + " / " + sdata.Length);
            if (acceptedChunck == previousAccepted) {
                //PROCESS
            }
            else {
                //RESET
                for (int i = 0; i < dataDebug.Length; i++) {
                    dataDebug[i] = 0f;
                }
            }
            //ADD NEW SAMPLE
            if (db > currentDB * 1.1f){
                float max = 0f;
                for (int i = 0; i < sdata.Length; i++) {
                    max += sdata[i];
                }
                max /= sdata.Length;
                for (int i = 0; i < sdata.Length; i++) {
                    dataDebug[i] += (float)Math.Sqrt(sdata[i] / max);
                }
            }
        }
        if (!acceptedChunck && previousAccepted) {
            Debug.Log("> nbChunck " + nbChunck);
            ApplyInstruction("INSTRUCTION_LISTEN_END");
            if(nbChunck >= 10){
                chunckWait = 20;
                Debug.Log("nbChunck " + nbChunck);
                //FINISH PROCESS
                float max = 0f;
                BlurSystem(2);
                //Copy blur system values
                for (int i = 0; i < dataDebug.Length; i++) {
                    dataDebug[i] = tmpDataDebug[i] / (float)(nbChunck);
                    max += dataDebug[i];
                }
                max /= dataDebug.Length;
                // Debug.Log(max);
                for (int i = 0; i < dataDebug.Length; i++) {
                    dataDebug[i] = (dataDebug[i] / max) * 0.01f;
                    data[i] = dataDebug[i];
                }
                //PROCESS TEST
                ProcessTestAction();
                //COPY END
                for (int i = 0; i < dataDebug.Length; i++) {
                    dataDebug_OLD[i] = dataDebug[i];
                }
            }

            nbChunck = 0;
        }

        if(!acceptedChunck){
            chunckWait--;
            nbChunck = 0;
        }
        previousAccepted = acceptedChunck;

        return average;
    }

    void ProcessTestAction()
    {
        if (!NeedTrain()) {
            string minKey = "RIEN WTF";
            float minVal = -1;
            foreach (var test in actionsModel) {
                float proba = probabiltySameAction(test.Value, dataDebug);
                if (minVal == -1 || proba < minVal) {
                    minKey = test.Key;
                    minVal = proba;
                }
                // Debug.Log("Value " + test.Key + " >>> " + proba);
            }
            Debug.Log("ESTIMATION : " + minKey + " >> " + minVal);
            lastconfidence = minVal;
            if (minVal <= confidence) {
                ApplyInstruction("VALID_INSTRUCTION");
                ApplyInstruction(minKey);
                // if (actionsModel[minKey].Count < 20)
                // {
                //     float[] addInstruction = new float[dataDebug.Length];
                //     System.Array.Copy(dataDebug, addInstruction, dataDebug.Length);
                //     actionsModel[minKey].Add(addInstruction);
                //     Debug.Log("Add instruction to " + minKey + " : " + actionsModel[minKey].Count);
                // }
            }else{
                ApplyInstruction("INVALID_INSTRUCTION");
            }
            //ApplyInstruction(minKey + " : " + minVal);
        }else{
            Train();
            if(!NeedTrain()){
                Debug.Log("TRAINING FINISHED");
                SaveVoiceOrder("default", actionsModel);
            }
        }
    }

    public void ResetTrain() {
        ApplyInstruction("Reset Train");
        foreach (var test in actionsModel) {
            test.Value.Clear();
        }
    }

    public void ResetLastTrain(){
        if(currentLearn != "" && actionsModel[currentLearn] != null && actionsModel[currentLearn].Count > 0){
            actionsModel[currentLearn].RemoveAt(actionsModel[currentLearn].Count - 1);
        }else{
            ResetTrain();
        }
    }

    bool NeedTrain() {
        foreach (var test in actionsModel) {
            if (test.Value.Count < trainTime) return true;
        }
        return false;
    }

    void Train(){
        foreach (var test in actionsModel)
        {
            if (test.Value.Count < trainTime) {
                float[] newDatas = new float[debugCount];
                for (int i = 0; i < dataDebug.Length; i++) {
                    newDatas[i] = dataDebug[i];
                }
                test.Value.Add(newDatas);
                currentLearn = test.Key;
                currentNumberLearn = test.Value.Count;
                ApplyInstruction("LEARN_INSTRUCTION");
                //ApplyInstruction(test.Key + " : " + test.Value.Count); //Fake instruction
                return;
            }
        }
    }

    float probabiltySameAction(List<float[]> models, float[] audio)
    {
        float averageValue = 0f;
        float minValue = -1f;
        for (int test = 0; test < models.Count; test++)
        {
            float totalDiff = 0;
            for (int i = 0; i < dataDebug.Length; i++) {
                totalDiff += (dataDebug[i] - models[test][i]) * (dataDebug[i] - models[test][i]);
            }
            // Debug.Log(totalDiff + " " + (totalDiff <= 0.4f ? " > MEME MOT !" : "different"));
            if (minValue == -1f || minValue > totalDiff) minValue = totalDiff;
            averageValue += totalDiff;
        }
        averageValue /= models.Count;
        return minValue;
    }

    void BlurSystem(int nbNeightboor) {
        for (int i = 0; i < tmpDataDebug.Length; i++)
        {
            float total = 0f;
            for (int j = -nbNeightboor; j <= nbNeightboor; j++) {
                int wantedPos = i + j;
                int pos = wantedPos < 0 ? 0 : wantedPos >= tmpDataDebug.Length ? tmpDataDebug.Length - 1 : wantedPos;
                total += dataDebug[pos];
            }
            tmpDataDebug[i] = total / (nbNeightboor * 2 + 1);
        }
    }
}
