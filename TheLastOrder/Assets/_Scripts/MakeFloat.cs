﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MakeFloat : MonoBehaviour
{

    [SerializeField, Range(0, 360)]
    float degreesPerSec = 35.0f;

    [SerializeField, Range(0, 2)]
    float amplitude = 0.1f;

    [SerializeField, Range(0.1f, 2.0f)]
    float frequency = 0.5f;

    Vector3 offset = new Vector3();
    Vector3 temp = new Vector3();

    // Start is called before the first frame update
    void Start()
    {
        offset = transform.localPosition; // get starting position;
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(new Vector3(0.0f, Time.deltaTime * degreesPerSec, 0.0f), Space.Self); // Spin around y axis.

        // Float with sin()
        temp = offset;
        temp.y += Mathf.Sin(Time.fixedTime * Mathf.PI * frequency) * amplitude + amplitude;
        transform.localPosition = temp;

    }
}
