﻿using System.Collections;
using System.Collections.Generic;
using System;
using TextSpeech;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using UnityEngine.Android;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class VoiceController : MonoBehaviour
{
    [System.Serializable]
    public struct Instruction {
        public List<string> name;
        public EventTrigger.TriggerEvent actions;
    }

    protected string microphone;
    const string LANG_CODE = "fr-FR";

    [SerializeField]
    Text uiTextSecure;
    [SerializeField]
    Text uiTextFast;
    [SerializeField]
    Text uiTextDEBUG;

    float timerRestart = 0.0f;
    float timeRestart = 0.1f;
    bool haveRestarted = true;

    float timeLoopControl = 0.5f;
    float timerLoopControl = 0.0f;

    bool isRunning = false;
    bool isRunningSave = false;
    bool speachRunning = false;
    bool speachRunningSave = false;

    float timeControlRunning = 5.0f;
    float timerControlRunning = 0.0f;

    float timeRemoveInstructionDebug = 3.0f;
    float timerRemoveInstructionDebug = 0.0f;

    //Instruction perceive
    string currentFinalText = "";
    string currentPartialText = "";
    string previousPartialText = "";

    //DictionnaryInstructions
    [SerializeField]
    public List<Instruction> instructions = new List<Instruction>();
    List<string> instructionDebugList = new List<string>();
    bool previousAccepted = false;
    int nbChunck = 0;
    int chunckWait = 0;

    List<GameObject> audioViewer = new List<GameObject>();
    List<GameObject> audioViewerSpectrum = new List<GameObject>();
    List<GameObject> debugAudio = new List<GameObject>();
    int debugCount = 512;
    float sizeDebug;
    [SerializeField] protected GameObject debugParent;
    float[] tmpDataDebug = new float[512];
    float[] dataDebug = new float[512];
    float[] dataDebug_OLD = new float[512];

    [SerializeField] protected GameObject spectrumParent;
    float[] sdata = new float[512];

    int wantedSpectrum = 64;
    float sizeSpectrum;

    int wantedCount = 10;
    float sizeRecord;
    protected AudioSource _audioSource;
    [SerializeField] protected GameObject sliderMicrophone;
    float currentValue = 0f;
    List<float> data = new List<float>();
    float[] dataTemp = new float[2048];

    Dictionary<string, List<float[]>> actionsModel = new Dictionary<string, List<float[]>>();
    
    void Awake(){
        sizeSpectrum = 1f / (wantedSpectrum);
        sizeRecord = 1f / (wantedCount);
        sizeDebug = 1f / (debugCount);

        actionsModel.Add("viens", new List<float[]>());
        actionsModel.Add("pars", new List<float[]>());
        actionsModel.Add("stop", new List<float[]>());
    }

    private void Start() {
//         Setup(LANG_CODE);

// #if UNITY_ANDROID
//         SpeechToText.instance.onPartialResultsCallback = OnPartialSpeechResult;
// #endif

//         TextToSpeech.instance.onStartCallBack = OnSpeakStart;
//         TextToSpeech.instance.onDoneCallback = OnSpeakStop;

//         SpeechToText.instance.onResultCallback = OnFinalSpeechResult;
//         SpeechToText.instance.onBeginningOfSpeechCallback = OnBeginningOfSpeach;
//         SpeechToText.instance.onErrorCallback = OnErrorCallback;
//         SpeechToText.instance.onEndOfSpeechCallback = OnEndOfSpeach;
//         SpeechToText.instance.onRmsChangedCallback = OnRmsChanged;
        // SpeechToText.instance.


        for(int i = 0; i < wantedCount; i++){
            GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
            cube.transform.parent = sliderMicrophone.transform;
            cube.transform.localPosition = new Vector3(i * sizeRecord - (wantedCount / 2f)*sizeRecord,0,0);
            cube.transform.localScale = new Vector3(sizeRecord,sizeRecord,sizeRecord);
            cube.transform.localRotation = Quaternion.identity;
            Destroy(cube.GetComponent<BoxCollider>());
            audioViewer.Add(cube);
            data.Add(0);
        }

        // for(int i = 0; i < sdata.Length; i++){
        //     GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
        //     cube.transform.parent = spectrumParent.transform;
        //     cube.transform.localPosition = new Vector3(i * sizeSpectrum - (wantedSpectrum / 2f) * sizeSpectrum, 0, 0);
        //     cube.transform.localScale = new Vector3(sizeSpectrum, sizeSpectrum, sizeSpectrum);
        //     cube.transform.localRotation = Quaternion.identity;
        //     Destroy(cube.GetComponent<BoxCollider>());
        //     audioViewerSpectrum.Add(cube);
        // }

        // for(int i = 0; i < dataDebug.Length; i++){
        //     GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
        //     cube.transform.parent = debugParent.transform;
        //     cube.transform.localPosition = new Vector3(i * sizeDebug - (dataDebug.Length / 2f) * sizeDebug, 0, 0);
        //     cube.transform.localScale = new Vector3(sizeDebug, sizeDebug, sizeDebug);
        //     cube.transform.localRotation = Quaternion.identity;
        //     Destroy(cube.GetComponent<BoxCollider>());
        //     debugAudio.Add(cube);
        // }

        CheckPermission();
        StartAudioMicrophone();
    }

    void StartAudioMicrophone(){
        _audioSource = GetComponent<AudioSource>();
        _audioSource.clip = Microphone.Start(null, true, 1, 44100);
        _audioSource.loop = true; // Set the AudioClip to loop
        while (!(Microphone.GetPosition(null) > 0)) { } // Wait until the recording has started
        _audioSource.Play();
    }

    void StopAudioMicrophone(){
        Microphone.End(null);
    }

    void Update(){
        GetAverageVolume();
        uiTextDEBUG.text = "";
        for(int i = 0; i < instructionDebugList.Count && i < 10; i++) uiTextDEBUG.text += instructionDebugList[i] + (i < instructionDebugList.Count - 1 ? "\n" : "");

        for (int i = 0; i < data.Count; i++) {
            data[i] = Math.Min(1, data[i]);
            data[i] = Math.Max(-1, data[i]);
            audioViewer[i].transform.localScale = new Vector3(sizeRecord, 10f * data[i], sizeRecord);
        }

        // for (int i = 0; i < sdata.Length; i++) {
        //     sdata[i] = Math.Min(1, sdata[i]);
        //     sdata[i] = Math.Max(-1, sdata[i]);
        //     audioViewerSpectrum[i].transform.localScale = new Vector3(sizeSpectrum, 30f * sdata[i], sizeSpectrum);
        // }

        // for (int i = 0; i < dataDebug.Length; i++) {
        //     dataDebug[i] = Math.Min(1, dataDebug[i]);
        //     dataDebug[i] = Math.Max(-1, dataDebug[i]);
        //     debugAudio[i].transform.localScale = new Vector3(sizeDebug, 30f * dataDebug[i], sizeDebug);
        // }

        timerRestart += Time.deltaTime;
        timerLoopControl += Time.deltaTime;

        if (instructionDebugList.Count > 0){
            timerRemoveInstructionDebug += Time.deltaTime;

            if (timerRemoveInstructionDebug >= timeRemoveInstructionDebug) {
                timerRemoveInstructionDebug = 0.0f;
                instructionDebugList.RemoveAt(0);
            }
        }

        for (int i = 0; i < Input.touchCount; ++i) {
            if (Input.GetTouch(i).phase.Equals(TouchPhase.Began)) {
                ResetTrain();
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }
        }

        // if(timerRestart >= timeRestart){
        //     if(!haveRestarted){
        //         StartListening();
        //         haveRestarted = true;
        //     }
        // }
        // if(timerLoopControl >= timeLoopControl){
        //     timerLoopControl = 0.0f;
        //     if(!isRunning){
        //         RestartRecording();
        //     }
        // }
        // if(speachRunning == speachRunningSave && isRunning == isRunningSave){
        //     timerControlRunning += Time.deltaTime;
        // }else{
        //     timerControlRunning = 0.0f;
        //     speachRunningSave = speachRunning;
        //     isRunningSave = isRunning;
        // }
        // if(timerControlRunning >= timeControlRunning){
        //     timerControlRunning = 0.0f;
        //     RestartRecording();
        // }
    }
    

    void CheckPermission()
    {
#if UNITY_ANDROID
        if (!Permission.HasUserAuthorizedPermission(Permission.Microphone))
        {
            Permission.RequestUserPermission(Permission.Microphone);
        }
#endif
    }

    #region Text to Speech
    public void StartSpeaking(string message)
    {
        TextToSpeech.instance.StartSpeak(message);
    }

    public void StopSpeaking()
    {
        TextToSpeech.instance.StopSpeak();
    }

    void OnSpeakStart()
    {
        Debug.Log("Talking started ...");
    }

    void OnSpeakStop()
    {
        Debug.Log("Talking stopped ...");
    }
    #endregion

    #region Speech to Text
    public void StartListening() {
        SpeechToText.instance.StartRecording();
        currentFinalText = "";
        currentPartialText = "";
        previousPartialText = "";
        uiTextSecure.text = "";
        uiTextFast.text = "";
    }
    public void StopListening() {
        SpeechToText.instance.StopRecording();
        isRunning = false;
        speachRunning = false;
    }

    void OnBeginningOfSpeach(){
        speachRunning = true;
    }

    void OnEndOfSpeach(){
        isRunning = false;
        speachRunning = false;
        //Restart to permit constant voice recognition
        StopListening();
        RestartRecording();
    }

    void OnRmsChanged(float value){
        timerControlRunning = 0.0f;
        timerRestart = 0.0f;
        timerLoopControl = 0.0f;
        isRunning = true;
    }

    void OnErrorCallback(string message){
        RestartRecording();
    }

    void OnFinalSpeechResult(string result) {
        if(result != ""){
            string[] previousInstructions = previousPartialText.Split(' ');
            string[] currentInstructions = result.Split(' ');
            int numberPrevious = previousInstructions.Length >= 0 && previousInstructions[0] == "" ? 0 : previousInstructions.Length;
            uiTextSecure.text = result + " " + previousInstructions.Length;
            int maxIterations = 4;
            for(int i = numberPrevious; i < currentInstructions.Length; i++){
                if(--maxIterations <= 0) break;
                ApplyInstruction(currentInstructions[i]);
            }
            currentFinalText = result;
        }
    }

    void OnPartialSpeechResult(string result) {
        if(result != ""){
            string[] currentInstructionFinal = currentFinalText.Split(' ');
            string[] previousInstructions = previousPartialText.Split(' ');
            string[] currentInstructions = result.Split(' ');
            int numberPrevious = previousInstructions.Length >= 0 && previousInstructions[0] == "" ? 0 : previousInstructions.Length;
            int numberCurrentText = currentInstructionFinal.Length >= 0 && currentInstructionFinal[0] == "" ? 0 : currentInstructionFinal.Length;
            int finalMax = Math.Max(numberPrevious, numberCurrentText);
            uiTextFast.text = previousInstructions.Length + " -> ";
            for(int i = finalMax; i < currentInstructions.Length; i++){
                uiTextFast.text += currentInstructions[i] + " ";
                ApplyInstruction(currentInstructions[i]);
            }
            previousPartialText = result;
        }
    }

    void ApplyInstruction(string instruction){
        instructionDebugList.Add(instruction);
        if(instructionDebugList.Count >= 5)
            instructionDebugList.RemoveAt(0);
        for(int i = 0; i < instructions.Count; i++){
            if(instructions[i].name.Contains(instruction)){
                BaseEventData eventData = new BaseEventData(EventSystem.current);
                eventData.selectedObject = this.gameObject;
                instructions[i].actions.Invoke(eventData);
                break;
            }
        }
    }

    void RestartRecording(){
        haveRestarted = false;
        timerRestart = 0.0f;
    }
    #endregion

    void Setup (string code)
    {
        TextToSpeech.instance.Setting(code, 1, 1);
        SpeechToText.instance.Setting(code);
    }

    float GetAverageVolume() {
        chunckWait--;
        _audioSource.GetOutputData(dataTemp, 0);
        _audioSource.GetSpectrumData(sdata, 0, FFTWindow.Hamming);
        bool acceptedChunck = false;
        float average = 0f;
        if (this.data.Count > 0) {
            for (int i = 0; i < dataTemp.Length; i++) {
                average += Math.Abs(dataTemp[i]);
            }
        }
        average /= dataTemp.Length;
        if (average > 0.025f && chunckWait <= 0) acceptedChunck = true;

        if(acceptedChunck){
            if(acceptedChunck == previousAccepted){
                //PROCESS
            }else{
                //RESET
                for(int i = 0; i < dataDebug.Length; i++){
                    dataDebug[i] = 0f;
                }
            }
            //ADD NEW SAMPLE
            for (int i = 0; i < sdata.Length; i++) {
                dataDebug[i] += sdata[i];
            }
        }
        if(!acceptedChunck && previousAccepted){
            chunckWait = 15;
            //FINISH PROCESS
            float max = 0f;
            BlurSystem(3);
            for (int i = 0; i < dataDebug.Length; i++) {
                dataDebug[i] = tmpDataDebug[i];
                if(dataDebug[i] > max) max = dataDebug[i];
            }
            // Debug.Log(max);
            for(int i = 0; i < dataDebug.Length; i++){
                dataDebug[i] = (dataDebug[i] / max) * 0.1f;
            }
            //PROCESS TEST
            ProcessTestAction();
            //COPY END
            for(int i = 0; i < dataDebug.Length; i++){
                dataDebug_OLD[i] = dataDebug[i];
            }
        }
        previousAccepted = acceptedChunck;
        return average;
    }

    void ProcessTestAction(){
        if(!NeedTrain()){
            string minKey = "RIEN WTF";
            float minVal = -1;
            foreach (var test in actionsModel) {
                float proba = probabiltySameAction(test.Value, dataDebug);
                if(minVal == -1 || proba < minVal){
                    minKey = test.Key;
                    minVal = proba;
                }
                // Debug.Log("Value " + test.Key + " >>> " + proba);
            }
            Debug.Log("ESTIMATION : " + minKey + " >> " + minVal);
            if(minVal <= 0.7){
                ApplyInstruction(minKey);
                if(actionsModel[minKey].Count < 20){
                    float[] addInstruction = new float[dataDebug.Length];
                    System.Array.Copy(dataDebug, addInstruction, dataDebug.Length);
                    actionsModel[minKey].Add(addInstruction);
                    Debug.Log("Add instruction to " + minKey + " : " + actionsModel[minKey].Count);
                }
            }
            ApplyInstruction(minKey + " : " + minVal);
        }
    }

    public void ResetTrain(){
        ApplyInstruction("Reset Train");
        foreach (var test in actionsModel) {
            test.Value.Clear();
        }
    }

    bool NeedTrain(){
        foreach (var test in actionsModel) {
            if(test.Value.Count < 3){
                float[] newDatas = new float[debugCount];
                for(int i = 0; i < dataDebug.Length; i++){
                    newDatas[i] = dataDebug[i];
                }
                test.Value.Add(newDatas);
                ApplyInstruction(test.Key + " : " + test.Value.Count);
                // Debug.Log("Ajout de l'audio à l'entrainement " + test.Key + " : " + test.Value.Count);
                return true;
            }
        }
        return false;
    }

    float probabiltySameAction(List<float[]> models, float[] audio){
        float averageValue = 0f;
        float minValue = -1f;
        for (int test = 0; test < models.Count; test++) {
            float totalDiff = 0;
            for (int i = 0; i < dataDebug.Length; i++) {
                totalDiff += Math.Abs(dataDebug[i] - models[test][i]);
            }
            // Debug.Log(totalDiff + " " + (totalDiff <= 0.4f ? " > MEME MOT !" : "different"));
            if(minValue == -1f || minValue > totalDiff) minValue = totalDiff;
            averageValue += totalDiff;
        }
        averageValue /= models.Count;
        return minValue;
    }

    void BlurSystem(int nbNeightboor){
        for (int i = 0; i < tmpDataDebug.Length; i++) {
            float total = 0f;
            for(int j = - nbNeightboor; j <= nbNeightboor; j++){
                int wantedPos = i + j;
                int pos = wantedPos < 0 ? 0 : wantedPos >= tmpDataDebug.Length ? tmpDataDebug.Length - 1 :  wantedPos;
                total += dataDebug[pos];
            }
            tmpDataDebug[i] = total / (nbNeightboor * 2 + 1);
        }
    }

    void OnAudioFilterRead(float[] data, int channels) {
        
        bool acceptedChunck = false;
        float average = 0f;
        if(this.data.Count > 0){
            int group = data.Length / this.data.Count;
            int posGroup = -1;
            for (int i = 0; i < data.Length; i++) {
                average += Math.Abs(data[i]);
                if (i % group == 0) {
                    if (posGroup >= 0) {
                        this.data[posGroup] /= group;
                    }
                    posGroup++;
                    if (posGroup < this.data.Count) this.data[posGroup] = 0;
                }
                if (posGroup < this.data.Count)this.data[posGroup] += data[i];
            }
        }

        average /= data.Length;
        if(average > 0.03f) acceptedChunck = true;

        if(acceptedChunck){
            if(acceptedChunck == previousAccepted){
                //PROCESS
            }else{
                //RESET
                // for(int i = 0; i < dataDebug.Length - 2048; i++){
                //     dataDebug[i] = 0f;
                // }
            }
            //ADD NEW SAMPLE
        }
        if(!acceptedChunck && previousAccepted){
            //FINISH PROCESS
        }
        
        // previousAccepted = acceptedChunck;
    }
}
