﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeColor : MonoBehaviour
{
    [SerializeField]
    Material unselected;
    [SerializeField]
    Material selected;

    public void Unselected()
    {
        GetComponent<Renderer>().material = unselected;
    }

    public void Selected()
    {
        GetComponent<Renderer>().material = selected;
    }


}
