﻿using System.Collections;
using UnityEngine;

[CreateAssetMenu()]
public class GameSettings : ScriptableObject
{
    [Header("Sound & Effect")]
    [Range(0.0f, 1.0f)]
    public float sfxVolume = 0.5f;
    [Range(0.0f, 1.0f)]
    public float musicVolume = 0.5f;

    [Header("Battery")]
    public int initialBatteryAmount = 0;
    public int currentBatteryAmount = 0;

    [Header("Life")]
    [Range(1, 15)]
    public int totalLifeAmount = 3;
    public int currentLifeAmount = 3;

    public void resetParameters()
    {
        currentBatteryAmount = initialBatteryAmount;
        currentLifeAmount = totalLifeAmount;
    }

    public void LoseLife()
    {
        if (currentLifeAmount - 1 >= 0)
        {
            currentLifeAmount--;
        }
    }

    public void AddBattery()
    {
        currentBatteryAmount++;
    }

}
