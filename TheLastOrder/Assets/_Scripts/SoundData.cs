﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu()]
public class SoundData : ScriptableObject
{
    [Header("Menu")]
    public AudioClip validate;
    public AudioClip back;
    public AudioClip collide;
    public AudioClip arrows;
    public AudioClip backgroundMenu;

    [Header("Collectible")]
    public AudioClip collect;

    [Header("Robot")]
    public AudioClip death;
    public AudioClip loseLife;
    public AudioClip spawn;

    [Header("Level Environment")]
    public AudioClip backgroundLevel;
}
