﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playSoundRobotSphere : MonoBehaviour
{
    public SettingsParameters settings;
    [SerializeField] AudioSource audioS;
    // Start is called before the first frame update
    void Start()
    {
        settings = GameObject.FindGameObjectWithTag("Settings").GetComponent<SettingsParameters>();
        audioS = GameObject.Find("AudioSourceHandler").GetComponent<AudioSource>();
    }

    public void playSpawnSound()
    {
        audioS.PlayOneShot(settings.SoundData.spawn, settings.GameSettings.sfxVolume);
    }
}
