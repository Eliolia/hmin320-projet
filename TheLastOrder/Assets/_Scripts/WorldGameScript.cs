﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldGameScript : MonoBehaviour
{
    // Start is called before the first frame update
    public Transform viewer;
    public Transform robot;
    
    void Start() {
        
    }

    // Update is called once per frame
    void LateUpdate() {
        float intensityDirection = Vector3.Dot(Vector3.down, viewer.forward); //[-1; 1] [Bottom, Top]
        float directionPercentage = 1f - (intensityDirection + 1) / 2f;
        Vector3 directionGround = new Vector3(viewer.forward.x, -0.1f, viewer.forward.z).normalized;
        transform.position = 
            viewer.position
            + Vector3.Lerp(viewer.forward, directionGround, Mathf.Clamp(directionPercentage * 1f, 0, 1)) * (3f + 2f * (Vector3.Dot(Vector3.down, viewer.forward) + 1))
            + Vector3.down * 1f
            - new Vector3(robot.transform.localPosition.x, robot.transform.localPosition.y > 0 ? robot.transform.localPosition.y : 0, robot.transform.localPosition.z);
    }
}
