﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GoNextLevelOnTrigger : MonoBehaviour
{
    protected float timer = 0.0f;
    [SerializeField] protected float timerEnd = 3.0f;
    [SerializeField] protected bool IsTrigger = false;
    [SerializeField] protected int level;
    [SerializeField] protected int maxLevel;


    // Start is called before the first frame update
    void Start()
    {
        timer = timerEnd;
        level = SceneManager.GetActiveScene().buildIndex + 1;
        maxLevel = SceneManager.sceneCountInBuildSettings;
    }

    // Update is called once per frame
    void Update()
    {
        if (IsTrigger)
        {
            timer -= Time.deltaTime;
            if (timer <= 0.0f)
            {
                SceneManager.LoadScene(level%maxLevel);
                IsTrigger = false;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Robot")
        {
            IsTrigger = true;
        }
    }
}
