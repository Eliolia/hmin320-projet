﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundButton : Activator
{
    [SerializeField] protected bool CanReset = false;

    protected bool oldActive;

    protected float timeAnimation = 0.5f;
    protected float timerAnimation = 0.0f;

    protected Vector3 previousPosition;
    protected Vector3 targetPosition;
    protected Vector3 initPosition;
    // Start is called before the first frame update
    void Start() {
        active = false;
        oldActive = active;
        initPosition = transform.localPosition;
        previousPosition = initPosition;
        targetPosition = initPosition;
    }

    // Update is called once per frame
    void Update() {
        timerAnimation += Time.deltaTime;
        if(timerAnimation >= timeAnimation) timerAnimation = timeAnimation;

        if(active != oldActive){
            previousPosition = transform.localPosition;
            if(active){
                //Need go down
                targetPosition = initPosition + new Vector3(0,-0.07f,0);
            }else{
                //Need go up
                targetPosition = initPosition;
            }
            timerAnimation = 0.0f;
        }

        transform.localPosition = Vector3.Lerp(previousPosition, targetPosition, Mathf.SmoothStep(0.0f, 1.0f, timerAnimation / timeAnimation));

        oldActive = active;
    }

    private void OnTriggerStay(Collider other) {
        if(other.tag == "Robot"){
            active = true;
        }
    }

    private void OnTriggerExit(Collider other) {
        if(other.tag == "Robot"){
            if(CanReset) active = false;
        }
    }
}
