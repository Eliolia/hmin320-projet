﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Activator : MonoBehaviour
{
    [SerializeField] protected bool active;

    public bool IsActive(){
        return active;
    }
}
