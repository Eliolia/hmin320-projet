﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    [SerializeField] protected List<Activator> activators;

    protected Vector3 basePosition;
    protected Vector3 openPosition;
    protected Vector3 baseRotation;
    [SerializeField] Vector3 relativeTargetOpen;
    [SerializeField] Vector3 openRotation = Vector3.zero;

    protected Vector3 transitionPosition;
    protected Vector3 transitionRotation;

    [SerializeField] protected float timeOpen = 1.0f;
    protected float timerOpen = 0.0f;

    [SerializeField] protected float delayTimeOpen = 0.0f;
    protected float delayTimerOpen = 0.0f;

    protected bool active = false;
    // Start is called before the first frame update
    void Start()
    {
        timerOpen = timeOpen;
        delayTimerOpen = delayTimeOpen;
        basePosition = transform.localPosition;
        openPosition = transform.localPosition + relativeTargetOpen;
        transitionPosition = transform.localPosition;
        baseRotation = transform.eulerAngles;
    }

    // Update is called once per frame
    void Update()
    {

        delayTimerOpen += Time.deltaTime;
        if(delayTimerOpen >= delayTimeOpen) delayTimerOpen = delayTimeOpen;

        bool currActive = AllActivatorActive(); 
        
        if(currActive != active){
            if(currActive){
                delayTimerOpen = delayTimeOpen * ( 1 - (timerOpen / timeOpen));
            }else{
                delayTimerOpen = delayTimeOpen * ( 1 - (timerOpen / timeOpen));
            }
            timerOpen = 0.0f;
            transitionPosition = transform.localPosition;
            transitionRotation = transform.eulerAngles;
        }
            
        active = currActive;

        if(delayTimerOpen >= delayTimeOpen){
            timerOpen += Time.deltaTime;
            if (timerOpen >= timeOpen) timerOpen = timeOpen;

            if(active){
                transform.localPosition = Vector3.Lerp(transitionPosition, openPosition, Mathf.SmoothStep(0.0f, 1.0f, timerOpen / timeOpen));
                transform.eulerAngles = Vector3.Lerp(transitionRotation, openRotation, Mathf.SmoothStep(0.0f, 1.0f, timerOpen / timeOpen));
            }else{
                transform.localPosition = Vector3.Lerp(transitionPosition, basePosition, Mathf.SmoothStep(0.0f,1.0f, timerOpen / timeOpen));
                transform.eulerAngles = Vector3.Lerp(transitionRotation, baseRotation, Mathf.SmoothStep(0.0f, 1.0f, timerOpen / timeOpen));
            }
        }
    }

    bool AllActivatorActive(){
        for(int i = 0; i < activators.Count; i++){
            if(!activators[i].IsActive()) return false;
        }
        return true;
    }
}
