﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ChangeValue : MonoBehaviour
{
    [SerializeField]
    bool IsIncrease;

    [SerializeField]
    GameObject tmp;
    
    [SerializeField]
    int step;

    SettingsParameters settings;
    private void Start()
    {
        settings = GameObject.FindGameObjectWithTag("Settings").GetComponent<SettingsParameters>();
    }

    TextMeshPro Value;
    public void SetValue()
    {
        Value = tmp.GetComponent<TextMeshPro>();
        if (IsIncrease)
        {
            increaseValue();
        }
        else { 
            decreaseValue();
        }
    }

    private void increaseValue()
    {
        int res = (int.Parse(Value.text) + step);
        if (res <= 100)
        {
            Value.text = "" + res;
            if(tmp.name == "VolumeLb")
            {
                settings.GameSettings.musicVolume += (float)step/ 100.0f;
            }
            if (tmp.name == "SFXLb")
            {
                settings.GameSettings.sfxVolume += (float)step / 100.0f;
            }
        }
    }

    private void decreaseValue()
    {
        int res = (int.Parse(Value.text) - step);
        if (res >= 0)
        {
            Value.text = "" + res;
            if (tmp.name == "VolumeLb")
            {
                settings.GameSettings.musicVolume -= (float)step / 100.0f;
            }
            if (tmp.name == "SFXLb")
            {
                settings.GameSettings.sfxVolume -= (float)step / 100.0f;
            }
        }
    }
}
