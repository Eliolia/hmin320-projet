﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class AudioSourceVolumeManager : MonoBehaviour
{
    [SerializeField] AudioSource audioS;
    public SettingsParameters settings;
    TextMeshProUGUI volumeLb;

    // Start is called before the first frame update
    void Start()
    {
        audioS = GameObject.Find("AudioSourceHandler").GetComponent<AudioSource>();
        settings = GameObject.FindGameObjectWithTag("Settings").GetComponent<SettingsParameters>();
        GameObject go = GameObject.Find("VolumeLb");
        if(go != null)
            volumeLb = go.GetComponent<TextMeshProUGUI>();
        audioS.volume = settings.GameSettings.musicVolume;
        if(volumeLb != null)
            volumeLb.text = (settings.GameSettings.musicVolume * 100.0f).ToString();
    }

    private void Update()
    {
        if(audioS.volume != settings.GameSettings.musicVolume)
        {
            audioS.volume = settings.GameSettings.musicVolume;
            if(volumeLb != null)
                volumeLb.text = (settings.GameSettings.musicVolume * 100.0f).ToString();
        }
    }
}
