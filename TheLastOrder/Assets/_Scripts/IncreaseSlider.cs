﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IncreaseSlider : MonoBehaviour
{
    public Slider slider;
    public float step;
    // Start is called before the first frame update
    void Start()
    {
        step = 0.1f;

    }

    public void increaseSliderFill()
    {
        if (slider.value + step <= 1.0f)
        {
            slider.value += step;
        }
    }
}
