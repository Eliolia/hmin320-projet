﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeMoving : MonoBehaviour
{
    Vector3 initialPosition;
    Rigidbody body;
    // Start is called before the first frame update
    void Start()
    {
        initialPosition = transform.localPosition;
        body = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if(transform.localPosition.y < initialPosition.y - 20){
            transform.localPosition = new Vector3(initialPosition.x, initialPosition.y + 1, initialPosition.z);
            body.angularVelocity = Vector3.zero;
            transform.rotation = Quaternion.identity;
            body.velocity = new Vector3(0,1f,0);
        }
    }
}
