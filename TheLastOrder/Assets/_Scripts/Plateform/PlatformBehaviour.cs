﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class PlatformBehaviour : MonoBehaviour
{
    [SerializeField] protected List<Transform> targets;
    [SerializeField] protected float speed = 2.0f;

    //TIMER STOP
    [SerializeField] protected float timeStop = 2.0f;
    protected float timerStop = 0.0f;
    protected int currentTargetIndex = 0;
    protected Transform currentTarget;

    protected Vector3 previousPosition;

    protected float timeChangePosition;
    protected float timerChangePosition;
    protected bool needRecomputeTime = true;

    protected Rigidbody body;

    protected List<Transform> movable = new List<Transform>();
    Vector3 lastPosition;

    // Start is called before the first frame update
    // v = d / t
    // d = v * t
    // t = d / v
    void Start()
    {
        for(int i = 0; i < targets.Count; i++) targets[i].gameObject.SetActive(false);
        if (targets.Count > 0) currentTarget = targets[0];
        body = GetComponent<Rigidbody>();
        lastPosition = transform.localPosition;
    }

    // Update is called once per frame
    void Update()
    {
        FollowTarget();

        Vector3 diff = transform.localPosition - lastPosition;

        if(movable.Count > 0){
            foreach (var item in movable) {
                item.position += diff;
            }
        }

        lastPosition = transform.localPosition;
    }

    void FollowTarget()
    {
        if (!currentTarget) return;

        timerStop += Time.deltaTime;
        if (timerStop >= timeStop) timerStop = timeStop;

        if(needRecomputeTime){
            timeChangePosition = Vector3.Distance(transform.localPosition, currentTarget.localPosition) / speed;
            previousPosition = transform.localPosition;
            timerChangePosition = 0.0f;
            needRecomputeTime = false;
        }

        if(!CanMove()){
            timerChangePosition = 0.0f;
            return; //We cant move, so we stop
        }

        timerChangePosition += Time.deltaTime;
        if(timerChangePosition >= timeChangePosition) timerChangePosition = timeChangePosition;

        transform.localPosition = Vector3.Lerp(previousPosition, currentTarget.localPosition, Mathf.SmoothStep(0,1,timerChangePosition / timeChangePosition));

        if(timerChangePosition >= timeChangePosition){
            timerStop = 0.0f;
            ChooseNextTarget();
        }
    }

    bool CanMove()
    {
        return timerStop >= timeStop;
    }

    void ChooseNextTarget()
    {
        currentTargetIndex++;
        if (currentTargetIndex >= targets.Count) currentTargetIndex = 0;
        currentTarget = targets[currentTargetIndex];
        needRecomputeTime = true;
    }

    private void OnTriggerStay(Collider other) {
        if(other.gameObject.tag == "Robot"){
            if(!movable.Contains(other.transform)){
                movable.Add(other.transform);
            }
        }
    }

    private void OnTriggerExit(Collider other) {
        movable.Remove(other.transform);
    }
}
