﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOnCollect : MonoBehaviour
{
    public Vector3 targetPosition = new Vector3(0.0f, 2.0f, 0.0f);
    public float timeMovement = 2.0f;
    bool isCollected = false;
    public float speed = 1.0f;
    public float startTime;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void Run()
    {
        startTime = Time.time;
        isCollected = true;
        Debug.Log("hit !");
    }

    // Update is called once per frame
    void Update()
    {
        if (isCollected)
        {
            if (Time.time - startTime < timeMovement) {
                Vector3.Lerp(transform.position, transform.position + targetPosition, 1.0f / timeMovement * Time.deltaTime);
            }
        }

        if(transform.position == targetPosition)
        {
            Destroy(this);
        }
    }
}
