﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RayCastNavigation : MonoBehaviour
{
    public Ray ray3;
    public RaycastHit2D hit3;
    public GameObject currentObject;
    bool raycastSelection = false;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        ray3 = new Ray(Camera.main.transform.position, Camera.main.transform.forward);
        hit3 = Physics2D.Raycast(ray3.origin, ray3.direction, Mathf.Infinity);
        if (hit3.collider != null)
        {
            currentObject = hit3.collider.gameObject;
            if (currentObject.tag == "ButtonsMenu")
            {
                Debug.Log("hit");
                //start coroutine
                if (Input.GetMouseButtonDown(0) || raycastSelection)
                {
                    currentObject.GetComponent<Button>().onClick.Invoke();
                }
            }
            else
            {
                //stop coroutine
            }
        }
        else
        {
            //stop coroutine
        }
    }

}
