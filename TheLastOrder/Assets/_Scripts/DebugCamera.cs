﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebugCamera : MonoBehaviour
{
    private Text text;
    private Camera cam;
    // Start is called before the first frame update

    private void Awake()
    {
        cam = Camera.main;
        text = gameObject.GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 position = cam.transform.position;
        Quaternion rotation = cam.transform.rotation;
        text.text = "Position (" + position.x + "," + position.y + "," + position.z + ")\nRotation (" + rotation.x + "," + rotation.y + "," + rotation.z + ")";   
    }
}
