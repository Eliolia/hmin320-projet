﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class EnemyBehaviour : MonoBehaviour
{
    [SerializeField] protected List<Transform> targets;
    [SerializeField] protected float speed = 2.0f;
    [SerializeField] protected float acceleration = 1.0f;

    [SerializeField] protected string TagPlayer = "Robot";
    
    protected Rigidbody body;

    protected float timeRotationSpeed = 0.5f;
    protected float timerRotationSpeed = 0.0f;

    //TIMER STOP
    [SerializeField] protected float timeStop = 2.0f;
    protected float timerStop = 0.0f;

    float currentSpeed = 0;

    protected int currentTargetIndex = 0;
    protected bool needNewLookAt = true;

    protected Transform currentTarget;

    protected Quaternion previousRotation;
    protected Quaternion targetRotation;

    protected
    // Start is called before the first frame update
    void Start() {
        for (int i = 0; i < targets.Count; i++) targets[i].gameObject.SetActive(false);
        previousRotation = transform.rotation;
        targetRotation = transform.rotation;

        body = GetComponent<Rigidbody>();
        if(targets.Count > 0) currentTarget = targets[0];
    }

    // Update is called once per frame
    void Update() {
        FollowTarget();
    }

    void FollowTarget(){
        if (!currentTarget) return;

        timerStop += Time.deltaTime;
        if(timerStop >= timeStop) timerStop = timeStop;

        if(CanMove()){
            currentSpeed += acceleration * Time.deltaTime;
            currentSpeed = currentSpeed > speed ? speed : currentSpeed;

            timerRotationSpeed += Time.deltaTime;
            if(timerRotationSpeed >= timeRotationSpeed) timerRotationSpeed = timeRotationSpeed;

            if (needNewLookAt) {
                previousRotation = transform.rotation;
                transform.LookAt(new Vector3(currentTarget.position.x, transform.position.y, currentTarget.position.z), Vector3.up);
                targetRotation = transform.rotation;
                timerRotationSpeed = 0.0f;
                needNewLookAt = false;
            }


            body.velocity = transform.forward * currentSpeed;
        }else{
            currentSpeed -= (speed * 2) * Time.deltaTime;
            currentSpeed = currentSpeed <= 0 ? 0 : currentSpeed;
            body.velocity = transform.forward * currentSpeed;
        }

        if(NeerTarget()){
            StopEnemy();
            ChooseNextTarget();
        }

        transform.rotation = Quaternion.Lerp(previousRotation, targetRotation, Mathf.SmoothStep(0.0f, 1.0f, timerRotationSpeed / timeRotationSpeed));
    }

    bool CanMove(){
        return timerStop >= timeStop;
    }

    bool NeerTarget(){
        return Vector3.Distance(transform.position, currentTarget.position) < 0.5;
    }

    void StopEnemy(){
        timerStop = 0.0f;
        needNewLookAt = true;
    }

    void ChooseNextTarget(){
        currentTargetIndex++;
        if(currentTargetIndex >= targets.Count) currentTargetIndex = 0;
        currentTarget = targets[currentTargetIndex];
        needNewLookAt = true;
    }

    private void OnCollisionEnter(Collision other) {
        if(other.gameObject.tag == TagPlayer){
            RobotBehaviour robot = other.gameObject.GetComponent<RobotBehaviour>();
            robot.Bump(transform.position);
        }
    }
}
