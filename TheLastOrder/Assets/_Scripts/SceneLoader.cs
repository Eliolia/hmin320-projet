﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    public Image progressBar;

    public void Start()
    {
        StartCoroutine(LoadAsyncOperations());
    }

    private IEnumerator LoadAsyncOperations()
    {
        yield return new WaitForSeconds(3.0f);

        AsyncOperation level = SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex + 1);

        while(level.progress < 1) // Tant qu'on a pas chargé le niveau
        {
            progressBar.fillAmount = level.progress;
            yield return new WaitForEndOfFrame();
        }
    }
}
