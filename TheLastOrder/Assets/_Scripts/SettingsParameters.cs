﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingsParameters : MonoBehaviour
{
    public GameSettings GameSettings;
    public SoundData SoundData;

    // Start is called before the first frame update
    void Start()
    {
        GameSettings.resetParameters();
    }
}
