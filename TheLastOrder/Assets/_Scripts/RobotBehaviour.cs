﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class RobotBehaviour : MonoBehaviour
{
    Rigidbody body;

    protected bool isDead = false;

    [SerializeField] protected float maxSpeed = 5f;
    [SerializeField] protected float timeAcceleration = 1f;
    protected float speed = 0f;

    [SerializeField] protected Animator robot_animator;

    //Time System for direction changement
    [SerializeField] protected float timeChangeDirection = 1f;
    protected float timerChangeDirection = 0f;
    protected bool haveChangeDirection = true;

    //Death animation
    [SerializeField] protected float timeResurect = 2f;
    protected float timerResurect = 0f;
    [SerializeField] protected float timeCanMove = 2f;
    protected float timerCanMove = 0f;

    //Accept order
    bool canMove = false;
    bool canAcceptOrder = true;

    protected bool bump = false; 

    protected float timerAcceleration = 1f;

    protected Quaternion savedLastDirection = Quaternion.identity;
    protected Quaternion wantedDirection = Quaternion.identity;

    public SettingsParameters settings;
    TextMeshProUGUI LbLife;
    TextMeshProUGUI LbBattery;
    LifeDisplay lifeDisplay;
    [SerializeField] AudioSource audioS;

    Vector3 respawnPosition = new Vector3(0,0.2f,0);

    // Start is called before the first frame update
    void Start() {
        body = GetComponent<Rigidbody>();
        settings = GameObject.FindGameObjectWithTag("Settings").GetComponent<SettingsParameters>();
        settings.GameSettings.resetParameters();
        // LbLife = GameObject.Find("LbLife").GetComponent<TextMeshProUGUI>();
        // LbBattery = GameObject.Find("LbBattery").GetComponent<TextMeshProUGUI>();
        audioS = GameObject.Find("AudioSourceHandler").GetComponent<AudioSource>();

        lifeDisplay = GameObject.Find("LIFE_DISPLAY").GetComponent<LifeDisplay>();
        // lifeDisplay.SetCurrentLife(settings.GameSettings.currentLifeAmount);

        // LbLife.text = settings.GameSettings.currentLifeAmount.ToString();
        // LbBattery.text = settings.GameSettings.currentBatteryAmount.ToString();
        timerChangeDirection = 0.0f;
        savedLastDirection = transform.rotation;
        wantedDirection = transform.rotation;
    }

    // Update is called once per frame
    void Update() {
        if(isDead){
            body.velocity = new Vector3(0,body.velocity.y,0);
            timerResurect += Time.deltaTime;
            savedLastDirection = Quaternion.identity;
            transform.rotation = savedLastDirection;
            
            if (timerResurect >= timeResurect){               
                settings.GameSettings.LoseLife();
                // LbLife.text = settings.GameSettings.currentLifeAmount.ToString();
                lifeDisplay.SetCurrentLife(settings.GameSettings.currentLifeAmount);
                if(settings.GameSettings.currentLifeAmount == 0) {
                    // Game Over
                    settings.GameSettings.resetParameters();
                    audioS.PlayOneShot(settings.SoundData.death, settings.GameSettings.sfxVolume);
                    SceneManager.LoadScene(0);
                    Debug.Log("lose");
                }
                body.velocity = new Vector3(0, 0, 0);
                isDead = false;
                transform.localPosition = respawnPosition;
                robot_animator.SetBool("Open_Anim", true);
            }
        }else{
            AliveBehaviour();
        }

    }

    public void playSpawnSound()
    {
        audioS.PlayOneShot(settings.SoundData.spawn, settings.GameSettings.sfxVolume);
    }

    void AliveBehaviour(){
        timerCanMove += Time.deltaTime;
        if(timerCanMove >= timeCanMove){
            timerCanMove = timeCanMove;
            canMove = true;
            robot_animator.SetBool("Open_Anim", true);
            if(bump){
                savedLastDirection = transform.rotation;
                wantedDirection = Quaternion.identity;
                timerChangeDirection = 0.0f;
                bump = false;
            }
        }else{
            canMove = false;
        }

        if (transform.localPosition.y <= -0.4) Fall();

        if(!canMove) return;

        timerChangeDirection += Time.deltaTime;
        if (timerChangeDirection >= timeChangeDirection) timerChangeDirection = timeChangeDirection;

        if(!haveChangeDirection && timerChangeDirection == timeChangeDirection){
            transform.rotation = wantedDirection;
            haveChangeDirection = true;
        }

        wantedDirection.eulerAngles = new Vector3(0, wantedDirection.eulerAngles.y, 0);

        //The robot have finish to rotate
        Vector3 direction = new Vector3(transform.forward.x, 0, transform.forward.z).normalized * speed;
        body.velocity = new Vector3(direction.x, body.velocity.y, direction.z);
        transform.rotation = Quaternion.Lerp(savedLastDirection, wantedDirection, timerChangeDirection / timeChangeDirection);

        if(body.velocity.magnitude > 0.2 || !haveChangeDirection){
            robot_animator.SetBool("Walk_Anim", true);
        }else{
            robot_animator.SetBool("Walk_Anim", false);
        }
    }

    void ResetAnimation(){
        robot_animator.SetBool("Roll_Anim", false);
        robot_animator.SetBool("Walk_Anim", false);
        robot_animator.SetBool("Open_Anim", false);
    }

    void Fall(){
        ResetAnimation();
        timerCanMove = 0f;
        canMove = false;
        isDead = true;
        timerResurect = 0f;
        speed = 0f;
        audioS.PlayOneShot(settings.SoundData.loseLife, settings.GameSettings.sfxVolume);
    }

    //Function to change the wanted direction to the camera direction
    void LookCameraDirection(){
        Vector3 CameraPosition = Camera.main.transform.position;
        Vector3 CameraPlan = new Vector3(CameraPosition.x, transform.position.y, CameraPosition.z);

        wantedDirection = Quaternion.LookRotation(CameraPlan - transform.position, Vector3.up);
    }

    //Function to reset the timer to update direction of the robot
    void ResetChangeDirection(){
        timerChangeDirection = 0f;                  //Reset timer
        savedLastDirection = transform.rotation;    //Save current rotation
        haveChangeDirection = false;                //Reset have change direction
    }

    bool AcceptOrders(){
        return canAcceptOrder && canMove;
    }

    //Instruction to go forward
    public void GoForward(){
        if(!AcceptOrders()) return;
        speed = maxSpeed;
        LookCameraDirection();
        ResetChangeDirection();
    }

    //Instruction to go backward
    public void GoBackward(){
        if (!AcceptOrders()) return;
        speed = maxSpeed;
        LookCameraDirection();
        wantedDirection *= Quaternion.Euler(0, 180f, 0);
        ResetChangeDirection();
    }

    //Instruction to stop the robot
    public void StopInstruction(){
        if (!AcceptOrders()) return;
        wantedDirection = transform.rotation;
        speed = 0f;
    }

    public void Bump(Vector3 positionBump){
        body.AddExplosionForce(100.0f, positionBump, 50.0f, 0.5f);
        canMove = false;
        timerCanMove = 0f;
        robot_animator.SetBool("Roll_Anim", false);
        robot_animator.SetBool("Walk_Anim", false);
        robot_animator.SetBool("Open_Anim", false);
        bump = true;
        speed = 0f;
    }

    private void OnTriggerEnter(Collider other) {
        if (other.gameObject.tag == "CheckPoint") {
            respawnPosition = transform.parent.InverseTransformPoint(other.transform.position);
        }
    }
}
