﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PopupArea : MonoBehaviour
{
    [TextArea]
    [SerializeField] protected string content;
    [SerializeField] protected TMP_Text text;

    [SerializeField] protected Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        text.text = content;
    }

    void ShowPopup(){
        animator.SetBool("Active", true);
    }

    void HidePopup(){
        animator.SetBool("Active", false);
    }

    private void OnTriggerEnter(Collider other) {
        if(other.gameObject.tag == "Robot"){
            ShowPopup();
        }
    }

    private void OnTriggerExit(Collider other) {
        if(other.gameObject.tag == "Robot"){
            HidePopup();
        }
    }
}
