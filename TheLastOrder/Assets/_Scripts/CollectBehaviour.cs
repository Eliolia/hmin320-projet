﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CollectBehaviour : MonoBehaviour
{
    SettingsParameters settings;
    TextMeshProUGUI LbBattery;
    GameObject collectible;
    AudioSource audioS;

    GameObject PILE_DISPLAY;

    private void Start()
    {
        settings = GameObject.FindGameObjectWithTag("Settings").GetComponent<SettingsParameters>();
        PILE_DISPLAY = GameObject.Find("PILE_DISPLAY");
        LbBattery = GameObject.Find("LbBattery").GetComponent<TextMeshProUGUI>();
        LbBattery.text = settings.GameSettings.currentBatteryAmount.ToString();
        audioS = GameObject.Find("AudioSourceHandler").GetComponent<AudioSource>();

        PILE_DISPLAY.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Battery")
        {
            PILE_DISPLAY.SetActive(true);
            collectible = other.gameObject;
            settings.GameSettings.AddBattery();
            audioS.PlayOneShot(settings.SoundData.collect, settings.GameSettings.sfxVolume);
            LbBattery.text = settings.GameSettings.currentBatteryAmount.ToString();
            Destroy(collectible);
        }
    }

}
