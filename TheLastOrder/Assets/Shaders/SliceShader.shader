﻿Shader "Custom/Slice"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
        _Glossiness ("Smoothness", Range(0,1)) = 0.5
        _Metallic ("Metallic", Range(0,1)) = 0.0

        sliceNormal1("normal1", Vector) = (0,0,-1,0)
        sliceCentre1("centre1", Vector) = (0,0,-2.5001,0)
        sliceNormal2("normal2", Vector) = (0,0,1,0)
        sliceCentre2("centre2", Vector) = (0,0,2.5001,0)
        sliceNormal3("normal3", Vector) = (1,0,0,0)
        sliceCentre3("centre3", Vector) = (2.5001,0,0,0)
        sliceNormal4("normal4", Vector) = (-1,0,0,0)
        sliceCentre4("centre4", Vector) = (-2.5001,0,0,0)

        sliceOffsetDst("offset", Float) = 0
    }
    SubShader
    {
        Tags { "Queue" = "Geometry" "IgnoreProjector" = "True"  "RenderType"="Geometry" }
        LOD 200

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard addshadow
        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0

        sampler2D _MainTex;

        struct Input
        {
            float2 uv_MainTex;
            float3 worldPos;
        };

        half _Glossiness;
        half _Metallic;
        fixed4 _Color;

        // World space normal of slice, anything along this direction from centre will be invisible
        float3 sliceNormal1;
        float3 sliceNormal2;
        float3 sliceNormal3;
        float3 sliceNormal4;
        // World space centre of slice
        float3 sliceCentre1;
        float3 sliceCentre2;
        float3 sliceCentre3;
        float3 sliceCentre4;
        // Increasing makes more of the mesh visible, decreasing makes less of the mesh visible
        float sliceOffsetDst;

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            float3 adjustedCentre1 = sliceCentre1 + sliceNormal1 * sliceOffsetDst;
            float3 offsetToSliceCentre1 = adjustedCentre1 - IN.worldPos;
            clip (dot(offsetToSliceCentre1, sliceNormal1));

            float3 adjustedCentre2 = sliceCentre2 + sliceNormal2 * sliceOffsetDst;
            float3 offsetToSliceCentre2 = adjustedCentre2 - IN.worldPos;
            clip (dot(offsetToSliceCentre2, sliceNormal2));

            float3 adjustedCentre3 = sliceCentre3 + sliceNormal3 * sliceOffsetDst;
            float3 offsetToSliceCentre3 = adjustedCentre3 - IN.worldPos;
            clip (dot(offsetToSliceCentre3, sliceNormal3));

            float3 adjustedCentre4 = sliceCentre4 + sliceNormal4 * sliceOffsetDst;
            float3 offsetToSliceCentre4 = adjustedCentre4 - IN.worldPos;
            clip (dot(offsetToSliceCentre4, sliceNormal4));
            
            // Albedo comes from a texture tinted by color
            fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
            o.Albedo = c.rgb;

            // Metallic and smoothness come from slider variables
            o.Metallic = _Metallic;
            o.Smoothness = _Glossiness;
            o.Alpha = c.a;
        }
        ENDCG
    }
    FallBack "VertexLit"
}
