﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/Section" {
 
    Properties {
        _Color1 ("Outside color", Color) = (1.0, 1.0, 1.0, 1.0)
        _Color2 ("Section color", Color) = (1.0, 1.0, 1.0, 1.0)
        _EdgeWidth ("Edge width", Range(0.9, 0.0)) = 0.9
        _Val_Min_X ("Min X value", float) = 0
        _Val_Max_X ("Max X value", float) = 0
        _Val_Min_Z ("Min Z value", float) = 0
        _Val_Max_Z ("Max Z value", float) = 0
    }
 
    SubShader {
        Tags { "Queue"="Geometry" }
 
        //  PASS 1
        CGPROGRAM
        #pragma surface surf Standard
 
        struct Input {
            float3 worldPos;
        };
 
        fixed4 _Color1;
        float _Val_Min_X;
        float _Val_Max_X;
        float _Val_Min_Z;
        float _Val_Max_Z;
 
        void surf(Input IN, inout SurfaceOutputStandard o)
        {
            if(IN.worldPos.x < _Val_Min_X || IN.worldPos.x > _Val_Max_X)
                discard;
            o.Albedo = _Color1;
        }
 
        ENDCG
 
        //  PASS 2
        Pass {
 
            Cull Front
 
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"
 
            struct v2f {
                float4 pos : SV_POSITION;
                float4 worldPos : TEXCOORD0;
            };
 
            v2f vert(appdata_base v)
            {
                v2f o;
                o.pos = UnityObjectToClipPos(v.vertex);
                o.worldPos = mul(unity_ObjectToWorld, v.vertex);
                return o;
            }
 
            fixed4 _Color2;
            float _Val_Min_X;
            float _Val_Max_X;
            float _Val_Min_Z;
            float _Val_Max_Z;
 
            fixed4 frag(v2f i) : SV_Target {
                if(i.worldPos.x < _Val_Min_X || i.worldPos.x > _Val_Max_X)
                    discard;
 
                return _Color2;
            }
 
            ENDCG
        }
 
        //  PASS 3
        Pass {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"
 
            struct v2f {
                float4 pos : SV_POSITION;
                float4 worldPos : TEXCOORD0;
            };
 
            float _EdgeWidth;
 
            v2f vert(appdata_base v)
            {
                v2f o;
                v.vertex.xyz *= _EdgeWidth;
                o.pos = UnityObjectToClipPos(v.vertex);
                o.worldPos = mul(unity_ObjectToWorld, v.vertex);
                return o;
            }
 
            fixed4 _Color2;
            float _Val_Min_X;
            float _Val_Max_X;
            float _Val_Min_Z;
            float _Val_Max_Z;
 
            fixed4 frag(v2f i) : SV_Target {
                if(i.worldPos.x < _Val_Min_X || i.worldPos.x > _Val_Max_X)
                    discard;
 
                return _Color2;
            }
 
            ENDCG
        }
 
        //  PASS 4
        Cull Front
 
        CGPROGRAM
        #pragma surface surf Standard vertex:vert
        struct Input {
            float3 worldPos;
        };
 
        float _EdgeWidth;
 
        void vert(inout appdata_base v)
        {
            v.vertex.xyz *= _EdgeWidth;
        }
 
        fixed4 _Color1;
        float _Val_Min_X;
        float _Val_Max_X;
        float _Val_Min_Z;
        float _Val_Max_Z;
 
        void surf(Input IN, inout SurfaceOutputStandard o)
        {
            if(IN.worldPos.x < _Val_Min_X || IN.worldPos.x > _Val_Max_X)
                discard;
 
            o.Albedo = _Color1;
        }
 
        ENDCG
    }
}